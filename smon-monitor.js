var _ = require('lodash');
var os = require('os');

var logger = require('./log.js');

var manager = require('./src/index.js');

var config;
try {
    config = require('./configs/config_dev.json');
}
catch (e) {
    config = require('./configs/config.json');
}

pck = require('./package.json')
var clientVersion  = {
    'str': pck.version
};

if (process.argv.indexOf("-key") != -1) {
    var key = process.argv[process.argv.indexOf("-key") + 1];
    var socket = require('socket.io-client')('http://' + config['smon_server']['ip'] + ':' + config.smon_server.port + '?api=' + key + '&v=' + pck.version);
} else {
    logger.error('You need to add -key');
    process.exit(0);
}

socket.on('connect', function () {
    logger.log('info', 'connected to smon-server');

    setInterval(interval, 5000);
    function interval() {
        socket.emit('client:version', clientVersion);
    }

    var PluginManager = new manager.PluginManager();
    PluginManager.on('get:data', function(data) {
        data['version'] = clientVersion;
        socket.emit('send:data', JSON.stringify(data));
    });

    PluginManager.on('policy:send', function(data) {
        data['version'] = clientVersion;
        logger.info('policy sent', data['type']);
        socket.emit('policy:send', JSON.stringify(data));
    });
    PluginManager.on('get:statistics', function(data) {
        data['version'] = clientVersion;
        socket.emit('get:statistics', JSON.stringify(data));
    });

    socket.on('smon:policies:send', function (data) {
        logger.log('info', 'Received policies from smon-server');
        PluginManager.handlePolicies(data);
    });

    socket.on('smon:error', function (data) {
        logger.log('info', data.message, data.params);
    });

    socket.on('smon:info', function (data) {
        logger.log('info', data.message, data.params);
    });

    socket.on('disconnect', function () {
        logger.info('Disconected from smon-server');
        process.exit();
    });
});
