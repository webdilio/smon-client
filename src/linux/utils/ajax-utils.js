var logger = require('winston');
/**
 Makes an AJAX-Call.
 @param url             URL to call
 @param callback        Callback function
 */
exports.xhrCall = function (url, callback) {
    var request = require("request");

    request({
        url: url,
        headers: { "Accept": "application/json" },
        method: "GET"
    }, function (error, response, body) {
        if(!error) {
            callback(body);
        } else {
            logger.error('Helper: XHR Error', {error:error});
        }
    });
};

/**
 Writes data to a JSON file and then returns the contents to the client.
 @param writePath         Path to store file
 @param dataToWrite        Data to write to file
 @param callback        The Callback (returns the contents as string)
 */
exports.writeToFile = function (writePath, dataToWrite, callback){
    var fs = require('fs-extra');

    fs.writeFile(writePath, dataToWrite, function(writeErr) {
        if (!writeErr) {
            fs.readFile(writePath, 'utf8', function (readErr, data) {
                if(!readErr){
                    callback(data);
                }else{
                    logger.error('Cannot read data', {error: readErr});
                }
            });
        } else {
            logger.error('Error getting data', {error: writeErr});
        }
    });
};
