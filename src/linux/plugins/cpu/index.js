var CpuManager = new (require(__dirname + '/manager/CpuManager'));

//var cpuM = new CpuManager;
var interval;

module.exports = function() {
    return {
        // package identity properties
        name: 'cpu',
        priority: 500, // default 100
        init: function() {
            return CpuManager.runStats(1000);
        },

        // register hooks callbacks
        'get_stats': function(callback) {
            CpuManager.on('get:stats', callback);
        }
    };
};