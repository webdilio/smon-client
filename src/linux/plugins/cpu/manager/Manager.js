var events = require("events");

function Manager() {
    events.EventEmitter.call(this);
}

Manager.prototype.__proto__ = events.EventEmitter.prototype;

module.exports = Manager;