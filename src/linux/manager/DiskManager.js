var shell = require('../utils/utils.js').shell;
var _ = require("lodash");
var fs = require('fs');

var Manager = require('../manager/Manager');
var DiskModel = require('../model/Disk');
var logger = require('../../../log');

/*
 * Columns for lsblk
 */
var columns = [
    'name',
    'kname',
    'label',
    'size',
    'state',
    'fstype',
    'model',
    'mountpoint',
    'type',
    'maj:min'
];


function DiskManager(CpuManager) {
    this.CpuManager = CpuManager;
    this.disks = [];
    this.disksTree = {};

    this.statistics = {
        'date': Date.now(),
        'data': {
            'date_start': Date.now(),
            'date_end': Date.now(),
            increments: 0,
            disks: {}
        }
    };

    this.init = false;
}
;


DiskManager.prototype = {
    getDisks: function () {
        logger.log('debug', 'getDisks', {plugin: 'disk'});
        var self = this;
        shell('lsblk', ['-Pnfbto', columns.join(',')]).fork(
                function (error) {
                    self.emit('error', {'plugin': 'disk', 'type': 'lsblk', err: error});
                },
                function (disks) {
                    var d = parse(disks);
                    var lastDisk = null;
                    var lastPart = null;
                    var lastRaid = null;
                    d.forEach(function (l, k) {
                        var id = l['kname'] + ':' + l['majorId'] + ':' + l['minorId'];
                        var diskM;
//                        var diskM = _.find(self.disks, function (chr) {
//                            return chr.id === id;
//                        });

                        if (typeof diskM === 'undefined') {
                            var diskM = new DiskModel(id, l['majorId'], l['minorId']);
                        }
                        diskM.updateDisk(l);
                        if (diskM.model.type === 'disk' || diskM.model.type === 'rom') {
                            //disk type, we create a new root type in witch we add the partitions and other subpartitions
                            lastDisk = diskM;
//                            self.disks.push(diskM);
                            self.disksTree[id] = diskM;
                        } else if (diskM.model.type === 'part') {
                            //is not disk we add the diskM into the last parent
                            lastPart = diskM;
                            lastDisk.addChild(diskM);
                        } else if (diskM.model.type.indexOf('raid') >= 0) {
                            lastRaid = diskM;
                            lastPart.addChild(diskM);
                        } else {
                            if(lastRaid) {
                                lastRaid.addChild(diskM);
                            } else if (lastPart) {
                                lastPart.addChild(diskM);
                            } else {
                                lastDisk.addChild(diskM);
                            }
                        }
                    });

                    self.emit('get:disks', self.disksTree);
                }
        );

        function parse(text) {
            return text.output
                    .trim()
                    .split(/\n/)
                    .map(function (line) {
                        var re = /([^=]*)=\"(.*?)\"/g, match, output = [];

                        while (match = re.exec(line)) {
                            output.push(match[2]);
                        }

                        var row = {};
                        columns.forEach(function (i, k) {
                            row[columns[k]] = output[k];
                            if (columns[k] === 'maj:min') {
                                row['majorId'] = output[k].split(':')[0];
                                row['minorId'] = output[k].split(':')[1];
                            }
                        });

                        return row;
                    });
        }
    },
    getDS: function () {
        var self = this;
        logger.log('debug', 'getDS', {plugin: 'disk'});
        shell('df', []).fork(
                function (error) {
                    self.emit('error', {'plugin': 'disk', 'type': 'df', err: error});
                },
                function (disks) {
                    var d = parse(disks.output);
                    d.forEach(function (l) {
                        traverseObject(self.disksTree, l);
                    });

                    self.sendStats(self.disksTree, false);
                    //self.emit('get:stats', self.disksTree);
                }
        );

        function traverseObject(obj, changes) {
            for (var key in obj) {
                if (obj.hasOwnProperty(key)) {
                    if(obj[key]['model']['mountpoint'] === changes['Mounted']) {
                       obj[key].updateDS(changes);
                    }
                    traverseObject(obj[key]['model']['children'], changes);
                }
            }
        };

        function parse(output) {
            if (!output) {
                return output;
            }
            var lines = output.split(/(\r?\n)/g);
            //console.log(lines[0]);
            var tableHead = [];
            var resultList = [];

            for (var i = 0; i < lines.length; i++) {
                // Process the line, noting it might be incomplete.
                //console.log(lines[i]);
                var parts = lines[i].split(/ +/g);
                var p = {};
                //console.log(parts) ;
                var add = false;
                if (parts.length > 1) {
                    for (var j = 0; j < parts.length; j++) {
                        if (i === 0) {
                            tableHead.push(parts[j]);
                        } else {
                            var th = tableHead[j];
                            p[th] = parts[j];
                            add = true;
                        }
                    }
                }
                if (add) {
                    resultList.push(p);
                }

            }
            return resultList;
        };
    },
    getIO: function () {
        logger.log('debug', 'getIO', {plugin: 'disk'});
        var self = this;
        fs.readFile('/proc/diskstats', function (err, buf) {
            if (err) {
                self.emit('error', {'plugin': 'disk', 'type': '/proc/diskstats', err: err});
            } else {
                var lines = buf.toString().trim().split("\n");
                lines.forEach(function (l) {
                    var ll = l.trim().split(/\s+/);
                    var id = ll[2] + ':' + l[0] + ':' + l[1];
                    traverseObject(self.disksTree, ll);
                });

                self.sendStats(self.disksTree, true);
            }
        });
        var tt = 0;
        function traverseObject(obj, changes) {
            for (var key in obj) {
                if (obj.hasOwnProperty(key)) {
                    var id = changes[2] + ':' + changes[0] + ':' + changes[1];
                    if(obj[key]['model']['id'] === id) {
                        obj[key].updateIO(changes, self.CpuManager.getDeltams());
                    }
                    traverseObject(obj[key]['model']['children'], changes);
                }
            }
        };
    },
    runStats: function (time) {
        var self = this;
        self.getDisks();
        self.on('get:disks', function (disks) {
            self.getDS();
            setInterval(interval, time);
            function interval() {
                self.getIO();
            }
            setInterval(ds, 10000);
            function ds() {
                self.getDS();
            }
        });

    }
};

DiskManager.prototype.sendStats = function(disksTree, io) {
    if(!this.init) {
        this.init = true;
    } else {
        var disks = {};
        var asd = _.clone(disksTree, true);
        traverseObject(asd, disks);

        function traverseObject(obj, disks) {
            for (var key in obj) {
                if (obj.hasOwnProperty(key)) {
                    disks[key] = _.clone(obj[key].model, true);
                    traverseObject(obj[key]['model']['children'], disks[key]['children']);
                }
            }
        };
    //    console.log(disksTree['sda:8:0']['model']['children']['sda1:8:1']);
    //    console.log(disksTree['sda:8:0']['model']['children']['sda1:8:1']);
        var data = {disks: disks, time: Date.now()};
        if(io) {
            var dd = {};
            getMountedDisks(disks, dd);
            this.setStatistics(dd);
        }

        this.emit('get:stats', data);
    }
};

/*
 * Set the statistics, we send them every minute
 * We need to calculate each start and end of a minute
 */
DiskManager.prototype.setStatistics = function(disks) {
    var self = this;
    self.statistics.date = new Date();
    if(self.statistics.date.getSeconds() == 0 && !self.statistics.init) {
        self.statistics.init = true;
        self.statistics.data.date_start = Date.now();
        self.statistics.data.increments = 0;
        for(var key in disks) {
            self.statistics.data.disks[disks[key].id] = {
                'id': disks[key].id,
                'mountpoint': disks[key]['mountpoint'],
                //
                'd_total': disks[key]['df']['total'], //total size of disk
                'd_available': disks[key]['df']['available'], //this is "available", need to change in the future
                'd_used': disks[key]['df']['used'],
                'd_usedp': disks[key]['df']['usedp'],
                //
                'high_writes': disks[key]['stats'].wr_ios,
                'avg_writes': 0,
                'low_writes': disks[key]['stats'].wr_ios,
                'total_writes': 0,
                //
                'high_reads': disks[key]['stats'].rd_ios,
                'avg_reads': 0,
                'low_reads': disks[key]['stats'].rd_ios,
                'total_reads': 0,
                //
                'high_iops': disks[key]['stats'].iops,
                'avg_iops': 0,
                'low_iops': disks[key]['stats'].iops,
                'total_iops': 0,
                // busy %
                'high_busy': disks[key]['stats'].busy,
                'avg_busy': 0,
                'low_busy': disks[key]['stats'].busy,
                'total_busy': 0
            };
        }
    }
    if(self.statistics.init) {
        for(var key in self.statistics.data.disks) {
            if(self.statistics.data.disks[key]['high_writes'] < disks[key]['stats']['wr_ios']) {
                self.statistics.data.disks[key]['high_writes'] = disks[key]['stats']['wr_ios'];
            }
            if(self.statistics.data.disks[key]['hight_reads'] < disks[key]['stats']['rd_ios']) {
                self.statistics.data.disks[key]['high_reads'] = disks[key]['stats']['rd_ios'];
            }
            if(self.statistics.data.disks[key]['high_iops'] < disks[key]['stats']['iops']) {
                self.statistics.data.disks[key]['high_iops'] = disks[key]['stats']['iops'];
            }
            if(self.statistics.data.disks[key]['high_busy'] < disks[key]['stats']['busy']) {
                self.statistics.data.disks[key]['high_busy'] = disks[key]['stats']['busy'];
            }

            if(self.statistics.data.disks[key]['low_writes'] > disks[key]['stats']['wr_ios']) {
                self.statistics.data.disks[key]['low_writes'] = disks[key]['stats']['wr_ios'];
            }
            if(self.statistics.data.disks[key]['low_reads'] > disks[key]['stats']['rd_ios']) {
                self.statistics.data.disks[key]['low_reads'] = disks[key]['stats']['rd_ios'];
            }
            if(self.statistics.data.disks[key]['low_iops'] > disks[key]['stats']['iops']) {
                self.statistics.data.disks[key]['low_iops'] = disks[key]['stats']['iops'];
            }
            if(self.statistics.data.disks[key]['low_busy'] > disks[key]['stats']['busy']) {
                self.statistics.data.disks[key]['low_busy'] = disks[key]['stats']['busy'];
            }

            self.statistics.data.disks[key]['total_writes'] = self.statistics.data.disks[key]['total_writes'] + disks[key]['stats']['wr_ios'];
            self.statistics.data.disks[key]['total_reads'] = self.statistics.data.disks[key]['total_reads'] + disks[key]['stats']['rd_ios'];
            self.statistics.data.disks[key]['total_iops'] = self.statistics.data.disks[key]['total_iops'] + disks[key]['stats']['iops'];
            self.statistics.data.disks[key]['total_busy'] = self.statistics.data.disks[key]['total_busy'] + disks[key]['stats']['busy'];
        }
        self.statistics.data.increments++;
        if(self.statistics.date.getSeconds() == 59) {
            self.statistics.init = false;
            self.statistics.data.date_end = Date.now();
            for(var key in self.statistics.data.disks) {
                self.statistics.data.disks[key]['avg_writes'] = _.round(self.statistics.data.disks[key]['total_writes'] / self.statistics.data.increments);
                self.statistics.data.disks[key]['avg_reads'] = _.round(self.statistics.data.disks[key]['total_reads'] / self.statistics.data.increments);
                self.statistics.data.disks[key]['avg_iops'] = _.round(self.statistics.data.disks[key]['total_iops'] / self.statistics.data.increments, 2);
                self.statistics.data.disks[key]['avg_busy'] = _.round(self.statistics.data.disks[key]['total_busy'] / self.statistics.data.increments, 2);
            }
            this.emit('get:statistics', self.statistics.data);
            self.statistics.data.disks = {};
        }
    }
};

DiskManager.prototype.__proto__ = Manager.prototype;

function getMountedDisks(obj, arr) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key)) {
            if(obj[key]['mountpoint'] !== '') {
                arr[obj[key].id] = obj[key];
            }
            getMountedDisks(obj[key]['children'], arr);
        }
    }
};

module.exports = DiskManager;
