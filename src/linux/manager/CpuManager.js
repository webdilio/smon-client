var _ = require('lodash');
var fs = require('fs');

var Manager = require('../manager/Manager');
var CpuModel = require('../model/Cpu');
var logger = require('../../../log');

var _cache = {
        'processors': {

        }
    };

function CpuManager() {
    this.processors = {'primary': null, 'cores': []};
    this.policies = {};
    this.statistics = {
        dateStart: new Date(),
        dateEnd: new Date(),
        init: false,
        data: {
            'percent': {
                'high': 0,
                'avg': 0,
                'low': 0,
                'total': 0,
                'increments': 0
            }
        }
    };
}

CpuManager.prototype.getProcessors = function () {
    logger.log('debug', 'getProcessors', {plugin: 'cpu'});
    var self = this;
    fs.readFile('/proc/stat', function (err, buf) {
        if (err){
            self.emit('error', {'plugin':'cpu', 'type': 'proc', err: err});
        } else {
            var lines = buf.toString().trim().split("\n");
            lines.forEach(function (l) {
                var p = l.indexOf(' ');
                var k = l.substr(0, p);
                var v = l.substr(p).trim();
                if (k.indexOf('cpu') === 0) {
                    //agregate processor
                    if('cpu' === k) {
                        if(self.processors.primary === null) {
                            cpu = new CpuModel(k, true);
                            cpu.updateProc(v.split(' '));
                            self.processors.primary = cpu;
                        } else {
                            self.processors.primary.updateProc(v.split(' '));
                        }
                        //set the policies
                    } else {
                        var cpu = _.find(self.processors.cores,  function(chr) {
                            return chr.model.id === k;
                        });
                        if(!cpu) {
                            cpu = new CpuModel(k, false);
                            cpu.updateProc(v.split(' '));
                            self.processors.cores.push(cpu);
                        } else {
                            cpu.updateProc(v.split(' '));
                        }
                    }
                }
            });
            
            self.sendStats(self.processors);
        }
    });
};

CpuManager.prototype.runStats = function(time) {
    var self = this;
    var int = setInterval(interval, time);
    function interval() {
        self.getProcessors();
    }
    
    return int;
};

CpuManager.prototype.getDeltams = function () {
    var ncpu = this.processors.cores.length;

    return this.processors.primary.model.usage.deltams / ncpu;
};

CpuManager.prototype.sendStats = function(processors) {
    var data = {primary: processors.primary.getModel(), cores: [], time: Date.now()};
    for(var key in processors.cores) {
        data['cores'].push(processors.cores[key].getModel());
    }
    
    this.checkPolicies();
    this.setStatistics(data.primary);
    
    this.emit('get:stats', data);
};

/*
 * Set the statistics, we send them every minute
 * We need to calculate each start and end of a minute 
 */
CpuManager.prototype.setStatistics = function(stats) {
    var self = this;
    self.statistics.date = new Date();
    if(self.statistics.date.getSeconds() == 0 && !self.statistics.init) {
        self.statistics.init = true;
        self.statistics.data.date_start = Date.now();
        self.statistics.data.percent.high = parseFloat(stats.usage.percent);
        self.statistics.data.percent.low = parseFloat(stats.usage.percent);
    }
    if(self.statistics.init) {
        /*
         * set the high usage
         */
        if(self.statistics.data.percent.high < parseFloat(stats.usage.percent)) {
            self.statistics.data.percent.high = parseFloat(stats.usage.percent);
        }
        /*
         * set the low usage
         */
        if(self.statistics.data.percent.low > parseFloat(stats.usage.percent)) {
            self.statistics.data.percent.low = parseFloat(stats.usage.percent);
        }
        /*
         * set the average usage
         */
        self.statistics.data.percent.total = _.round(self.statistics.data.percent.total, 2) + parseFloat(stats.usage.percent);
        self.statistics.data.percent.increments++;
        if(self.statistics.date.getSeconds() == 59) {
            self.statistics.init = false;
            /*
             * we send the data now and we reset
             */
            self.statistics.data.date_end = Date.now();
            self.statistics.data.percent.avg = _.round(self.statistics.data.percent.total / self.statistics.data.percent.increments, 2)
            this.emit('get:statistics', self.statistics.data);
            self.statistics.data = {
                'percent': {
                    'high': 0,
                    'avg': 0,
                    'low': 0,
                    'total': 0,
                    'increments': 0
                }
            };
        }
    }
};

CpuManager.prototype.setPolicies = function(policies) {
    var self = this;
    policies.forEach(function(policy) {
        if(!self.policies[policy.id]) {
            self.policies[policy.id] = policy;
        } else {
            policy['status'] = self.policies[policy.id]['status'];
            self.policies[policy.id] = policy;
        }
    });
};

CpuManager.prototype.checkPolicies = function() {
    var self = this;
    
    for(var pid in self.policies) {
        if (self.processors.primary.model.usage.percent >= self.policies[pid]['cpu_open_percent'] && (self.policies[pid]['status'] === 'closed' || self.policies[pid]['status'] === 'not_verified')) {
            //open the policy and add the incident
            self.policies[pid]['status'] = 'opened';
            var cpuLoad = {'policy': self.policies[pid], 'data': self.processors.primary.getModel(), 'status': 'open', type: 'cpu_load', time: Date.now()};
            self.emit('policy:send', cpuLoad);
        }
        if (self.processors.primary.model.usage.percent < self.policies[pid]['cpu_close_percent'] && (self.policies[pid]['status'] === 'opened' || self.policies[pid]['status'] === 'not_verified')) {
            //close the policy and close the incident
            self.policies[pid]['status'] = 'closed';
            var cpuLoad = {'policy': self.policies[pid], 'data': self.processors.primary.getModel(), 'status': 'close', type: 'cpu_load', time: Date.now()};
            self.emit('policy:send', cpuLoad);
        }
    }
};

CpuManager.prototype.__proto__ = Manager.prototype;
//
//var CpuManager = new CpuManager();
//CpuManager.runStats(1000);
//var proc = process;
//
////process.send({ foo: 'bar' });
//process.on('message', function(m) {
//  console.log('CHILD got message:', m);
//  CpuManager.on('get:stats', function(data) {
//        process.nextTick(function () {
//        process.send(data);
//      });
//
//    });
//});


module.exports = CpuManager;