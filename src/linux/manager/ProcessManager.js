var shell = require('../utils/utils.js').shell;
var _ = require("lodash");
var fs = require('fs');
var crypto = require('crypto');

var Manager = require('../manager/Manager');
var ProcessModel = require('../model/Process');
var logger = require('../../../log');

/*
 * Columns for ps
 */
var columns = [
    'pid',
    'ppid',
    'user:50',
    'pcpu',
    'pmem',
    'vsz',
    'rss',
    'stat',
    'stime',
    'time',
    'comm',
    'command'
];


function ProcessManager() {
    this.processes = {};
    this.statistics = {
        'date': Date.now(),
        'data': {
            'date_start': Date.now(),
            'date_end': Date.now(),
            processes: {}
        }
    }
};


ProcessManager.prototype = {
    getProcesses: function () {
        logger.log('debug', 'getProcesses', {plugin: 'process'});
        var self = this;
        shell('ps', ['axo', columns.join(','), '--sort=-pmem,-pcpu']).fork(
            function (error) {
                self.emit('error', {'plugin': 'process', 'type': 'ps', err: error});
            },
            function (output) {
                self.processes = {};
                parse(output);

                self.sendStats(self.processes);
            }
        );

        function parse(text) {
            return text.output
                    .trim()
                    .split(/\n/)
                    .slice(1)
                    .map(function (line) {
                        var output = line.split(' ').map(function(item) {
                            if(item !== '') {
                                return item;
                            } else {
                                return null;
                            }
                        })
                        .filter(Boolean);

                        var row = {};
                        columns.forEach(function(i,k){
                          var col = columns[k];
                          if(columns[k] === 'user:50') {
                            col = 'user';
                          }
                            row[col] = output[k];
                        });
                        if(row['ppid'] != 2) {
                            var key = row['user'] + row['comm'];
                            var hash = crypto.createHash('md5').update(key).digest('hex');
                            if(!self.processes[hash]) {
                                self.processes[hash] = new ProcessModel(hash);
                                self.processes[hash].update(row, false);
                            } else {
                                self.processes[hash].update(row, true);
                            }

                            return self.processes[hash];
                        }
//                        if(!self.processes[row['pid']]) {
//                            self.processes[row['pid']] = new ProcessModel();
//                        }
//
//                        self.processes[row['pid']].update(row);
//
//                        return self.processes[row['pid']];
                    });
        }
    },
    runStats: function (time) {
        var self = this;
        self.getProcesses();
        var int = setInterval(interval, time);
        function interval() {
            self.getProcesses();
        }

        return int;
    }
};

ProcessManager.prototype.sendStats = function(processes) {
    var data = {processes: {}, time: Date.now()};
    for(var key in processes) {
        data['processes'][key] = processes[key].getModel();
    }
//console.log(data.processes)    ;die;
    this.checkPolicies();

    this.setStatistics(data['processes']);

    this.emit('get:stats', data);
};

/*
 * Set the statistics, we send them every minute
 * We need to calculate each start and end of a minute
 */
ProcessManager.prototype.setStatistics = function(stats) {
    var self = this;
    self.statistics.date = new Date();

    self.statistics.data.date_start = Date.now() - self.statistics.date.getSeconds()*1000;
    self.statistics.increments = 1;
    self.statistics.data.date_end = Date.now() + (59 - self.statistics.date.getSeconds())*1000;

    self.statistics.data.processes = stats;
    self.emit('get:statistics', self.statistics.data);
};

ProcessManager.prototype.setPolicies = function(policies) {
    var self = this;
    self.policies = {};
    policies.forEach(function(policy) {
        if(!self.policies[policy.id]) {
            self.policies[policy.id] = policy;
        } else {
            policy['status'] = self.policies[policy.id]['status'];
            self.policies[policy.id] = policy;
        }
    });
};

ProcessManager.prototype.checkPolicies = function() {
    var self = this;
    for(var pid in self.policies) {
        var trigger = 1;
        var proc = [];
        for(var prid in self.processes) {
            if (self.processes[prid].model.comm.toLowerCase() === self.policies[pid]['process_name'].toLowerCase() && self.processes[prid].model.user.toLowerCase() === self.policies[pid]['process_user'].toLowerCase()) {
                trigger = 0;
                proc.push(self.processes[prid]);
            }
        };
        if (trigger == 1 && (self.policies[pid]['status'] === 'closed' || self.policies[pid]['status'] === 'not_verified')) {
           self.policies[pid]['status'] = 'opened';
            var processRunning = {'policy': self.policies[pid], 'data': proc, 'status': 'open', type: 'process_running', time: Date.now()};

            self.emit('policy:send', processRunning);
        }
        if (trigger == 0 && (self.policies[pid]['status'] === 'opened' || self.policies[pid]['status'] === 'not_verified')) {
            self.policies[pid]['status'] = 'closed';
            var processRunning = {'policy': self.policies[pid], 'data': proc, 'status': 'close', type: 'process_running', time: Date.now()};

            self.emit('policy:send', processRunning);
        }
    }
};

ProcessManager.prototype.__proto__ = Manager.prototype;

module.exports = ProcessManager;
