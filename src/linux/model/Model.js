var events = require("events");
var _ = require('lodash');

function Model() {
    events.EventEmitter.call(this);
}

Model.prototype.get = function (attribute) {
    var attrs = attribute.split('.');
    var ret = this.model;
    attrs.forEach(function(v, k) {
        ret = ret[v];
    });
    
    return ret;
};

Model.prototype.set = function (attribute, value) {
    this.model[attribute] = value;
};

Model.prototype.getModel = function () {
    return this.model;
};

Model.prototype.__proto__ = events.EventEmitter.prototype;

module.exports = Model;