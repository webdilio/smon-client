#!/bin/bash
#hmm 1 2 3 4
Arch=$(uname -m | sed 's/x86_//;s/i[3-6]86/32/')

get_distribution_type()
{
    local dtype
    # Assume unknown
    dtype="unknown"

    # First test against Fedora / RHEL / CentOS / generic Redhat derivative
    if [ -r /etc/rc.d/init.d/functions ]; then
        source /etc/rc.d/init.d/functions
        [ zz`type -t passed 2>/dev/null` == "zzfunction" ] && dtype="redhat"

    # Then test against SUSE (must be after Redhat,
    # I've seen rc.status on Ubuntu I think? TODO: Recheck that)
    elif [ -r /etc/rc.status ]; then
        source /etc/rc.status
        [ zz`type -t rc_reset 2>/dev/null` == "zzfunction" ] && dtype="suse"

    # Then test against Debian, Ubuntu and friends
    elif [ -r /lib/lsb/init-functions ]; then
        source /lib/lsb/init-functions
        [ zz`type -t log_begin_msg 2>/dev/null` == "zzfunction" ] && dtype="debian"

    # Then test against Gentoo
    elif [ -r /etc/init.d/functions.sh ]; then
        source /etc/init.d/functions.sh
        [ zz`type -t ebegin 2>/dev/null` == "zzfunction" ] && dtype="gentoo"

    # For Slackware we currently just test if /etc/slackware-version exists
    # and isn't empty (TODO: Find a better way :)
    elif [ -s /etc/slackware-version ]; then
        dtype="slackware"
    fi
    echo $dtype
}


distro=get_distribution_type

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

wget=/usr/bin/wget
tar=/bin/tar
nodejs=/usr/bin/node
npm=/usr/bin/npm
working_dir="$( cd -P "$(dirname "$DIR/../../" )" && pwd )"
download_dir="$working_dir/install"
arh_file="$download_dir/smon-monitor.tar.gz"
key_file="$working_dir/configs/key"
run_file="$working_dir/smon-server.js"
url_base="https://bitbucket.org/webdilio/smon-client/get/master.tar.gz"
wget_opt=" -O $download_dir/smon-monitor.tar.gz"
key=""


if [ -f "$working_dir/pids/server.pid" ]; then
    monitor_pid=`cat "$working_dir/pids/server.pid"`
fi
if [ -f "$working_dir/configs/key" ]; then
    key=`cat "$working_dir/configs/key"`
fi
#we kill the processes
kill $(ps aux | grep 'smon-server.js' | awk '{print $2}') &>/dev/null
kill $(ps aux | grep 'smon-monitor.js' | awk '{print $2}') &>/dev/null

rm -rf "$working_dir/configs/"*
rm -rf "$working_dir/install/"*
rm -rf "$working_dir/pids/"*
rm -rf "$working_dir/sock"
rm -rf "$working_dir/src"
rm -rf "$working_dir/log.js"
rm -rf "$working_dir/package.json"
rm -rf "$working_dir/smon-monitor.js"
rm -rf "$working_dir/smon-server.js"
rm -rf "$working_dir/bin/"*
rm -rf "$working_dir/node_modules"

echo "Removed all data" >&2
#rm -rf "$download_dir/webdilio-smon-client"*

if [ ! -x "$wget" ]; then
  echo "ERROR: No wget." >&2
  exit 1
elif [ ! -x "$tar" ]; then
  echo "ERROR: No tar." >&2
  exit 1
fi

#
# We delete the archive first
#
rm -rf "$download_dir/smon-monitor.tar.gz"

#
#   We download
#
if ! $wget $wget_opt "${url_base}"; then
  echo "ERROR: can't get archive" >&2
  exit 1
fi

# extract archive
if [ ! -f "$download_dir/smon-monitor.tar.gz" ]; then
  echo "ERROR: I'm confused, where's my archive?" >&2
  exit 1
fi

echo "untaring $download_dir/smon-monitor.tar.gz -C $download_dir"
tar zxf "$arh_file" -C "$download_dir"

cp -R "$download_dir/webdilio-smon-client"*/* "$working_dir"

#lets copy the apikey now

echo $1
rm -rf $key_file

echo "$key" > "$working_dir/configs/key"

rm -rf "$download_dir/webdilio-smon-client"*

#lets install nodejs and npm
#if [ ! -f "$nodejs" ]; then
#
#fi

$npm "install" "$working_dir/" "--prefix" "$working_dir/"
rm "/etc/init.d/smond"

ln -s "$working_dir/bin/smond" "/etc/init.d/smond"
chmod 777 "/etc/init.d/smond"

echo "starting smond ..." >&2

$nodejs "$run_file" < /dev/null &>/dev/null &

echo "OK"

disown
